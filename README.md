# RED BICICLETAS

## Introducción

Proyecto RED BICICLETAS COURSERA.

## Update Endpoint POSTMAN
Del tipo PUT
http://localhost:3000/api/bicicletas/update/3

{
    "id": 3,
    "color": "negra-2",
    "modelo": "montaña-2",
    "ubicacion": [
        0.347249,
        -78.130199
    ]
}

## Delete Endpoint POSTMAN
Del tipo DELETE
http://localhost:3000/api/bicicletas/delete

Body
{
    "id": 1
}

