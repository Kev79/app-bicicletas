console.log("Hola");
var map = L.map('main_map').setView([0.3483598,-78.130623,17], 13);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia2V2aW43OSIsImEiOiJja2c0N2p3aDMwaHV5MnJvM3lyaGp5amx3In0.MLjoqg4iEi9vv4BYEqfqFQ'
}).addTo(map);

//var marker = L.marker([0.348955, -78.132329]).addTo(map);
//var marker = L.marker([0.348574, -78.131149]).addTo(map);

$.ajax({
    dataType : "json",
    url: "api/bicicletas",
    success: function (result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})
