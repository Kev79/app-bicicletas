var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){   
    Bicicleta.find({}, function(err, bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.findById(req.params.code);
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.ubicacion[0], req.body.ubicacion[1]];
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.code);
    res.status(204).send();
}

