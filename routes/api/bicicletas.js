var express = require('express');
var router = express.Router();
var bicicletaController = require("../../controllers/api/bicicletaControllerAPI");

/* GET lista bicicletas. */
router.get('/', bicicletaController.bicicleta_list);

router.post('/create', bicicletaController.bicicleta_create);

router.put('/update/:id', bicicletaController.bicicleta_update);

//router.get('/:id/update', bicicletaController.bicicleta_update_get);

//router.post('/:id/update', bicicletaController.bicicleta_update_post);

router.delete('/delete', bicicletaController.bicicleta_delete);

module.exports = router;